var App = React.createClass({
    
    getInitialState: function() {
        return {
            searchResults: ''
        }
    },

    showResults: function(response){
        this.setState({
            searchResults: response
        })
    },
    
    search: function(URL){
        $.ajax({
            type: "GET",
            dataType: 'jsonp',
            url: URL,
            success: function(response){
                this.showResults(response);
            }.bind(this)
        });
    },

    render: function(){
        return (
            <div>
                <SearchBox search={this.search} />
                <Results searchResults={this.state.searchResults} />
            </div>
        );
    },


});

var SearchBox = React.createClass({
    
    render: function(){
        return (
            <div>
                <input type="text" ref="query" />
                <input type="submit" onClick={this.createAjax} value="Buscar" />
            </div>
        );
    },

    createAjax: function(){
        var query    = ReactDOM.findDOMNode(this.refs.query).value;
        var URL      = 'https://viacep.com.br/ws/' + query +'/json/?callback=myfn';
        this.props.search(URL)
    }

});

var Results = React.createClass({
    
    render: function(){
        // var resultItems = this.props.searchResults.map(function(result) {
        //     return <ResultItem key={result.trackId} trackName={result.trackName} />
        // });
        return(
            <div>
                {this.props.searchResults.bairro}
            </div>           
        );
    }
});

// var ResultItem = React.createClass({
    
//     render: function(){
//         return <li>{this.props.trackName}</li>;
//     }
// });

ReactDOM.render(<App />,  document.getElementById("content"));